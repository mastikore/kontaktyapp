# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table contact (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  surname                   varchar(255),
  company                   varchar(255),
  email                     varchar(255),
  telephone                 varchar(255),
  constraint pk_contact primary key (id))
;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists contact;

SET REFERENTIAL_INTEGRITY TRUE;

