# --- Sample dataset

# --- !Ups

insert into contact (id,name,surname,company,email,telephone) values (1,'arkadiusz','babacki','firma a','ababacki@gmail.com','111222333');
insert into contact (id,name,surname,company,email,telephone) values (2,'bartosz','cabacki','firma b','bcabacki@gmail.com','444555666');
insert into contact (id,name,surname,company,email,telephone) values (3,'czeslaw','dabacki','firma c','cdabacki@gmail.com','777888999');
insert into contact (id,name,surname,company,email,telephone) values (4,'daniel','ebacki','firma d','debacki@gmail.com','112233445');
insert into contact (id,name,surname,company,email,telephone) values (5,'ernest','fabacki','firma e','efabacki@gmail.com','566778899');

# --- !Downs

delete from contact;