$(document).ready(function () {
    function checkName(){
  	    //get corresponding form element
  	    var isCorrect = false;
  	 	if(document.getElementById("contactName").value.length<3)	{
  	 	    alert("Error: FirstName should be more than 3 characters!");
  	 		document.getElementById("contactName").focus();
  	 	}else {
  	 	    re = /^\w+$/;
  		    if(!re.test(document.getElementById("contactName").value)) {
  			alert("Error: FirstName must contain only letters, numbers and underscores!");
  			document.getElementById("contactName").focus();
            }else{
                isCorrect = true;
            }
        }
        return isCorrect;
    }
    function checkSurname(){
        //get corresponding form element
        var isCorrect = false;
     	if(document.getElementById("contactSurname").value.length<3)	{
     	    alert("Error: LastName should be more than 3 characters!");
     		document.getElementById("contactSurname").focus();
     	}else {
     	    re = /^\w+$/;
     	    if(!re.test(document.getElementById("contactSurname").value)) {
     		alert("Error: LastName must contain only letters, numbers and underscores!");
     		document.getElementById("contactSurname").focus();
            }else{
                isCorrect = true;
            }
        }
        return isCorrect;
    }
    function checkCompany(){
        //get corresponding form element
        var isCorrect = false;
     	if(document.getElementById("contactCompany").value.length<3)	{
     	    alert("Error: CompanyName should be more than 3 characters!");
     		document.getElementById("contactCompany").focus();
     	}else {
     	    re = /^\w+$/;
     	    if(!re.test(document.getElementById("contactCompany").value)) {
     		alert("Error: CompanyName must contain only letters, numbers and underscores!");
     		document.getElementById("contactCompany").focus();
            }else{
                isCorrect = true;
            }
        }
        return isCorrect;
    }
    function checkEmail(){
        //get corresponding form element
        var isCorrect = false;
     	re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
     	if(!re.test(document.getElementById("contactEmail").value)) {
     		alert("Error: ContactEmail is not an email address!");
     		document.getElementById("contactEmail").focus();
        } else {
            isCorrect = true;
        }
        return isCorrect;
    }
    function checkTelephone(){
        //get corresponding form element
        var isCorrect = false;
     	re = /^\d{3}-\d{3}-\d{3}|\d{9}$/;
     	if(!re.test(document.getElementById("contactTelephone").value)) {
     		alert("Error: ContactPhone must consists of 9 digits!\nAllowed formats:\nXXX-XXX-XXX\nXXXXXXXXX");
     		document.getElementById("contactTelephone").focus();
        } else {
            isCorrect = true;
        }
        return isCorrect;
    }/*
    $(".editButton").click(function() {
            $("#contact_modal").open("lg");
    });*/
});