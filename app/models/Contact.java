package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.*;

/**
 * Created by lazyFace on 2015-02-25.
 */
@Entity
public class Contact extends Model{

    /*private static final long serialVersionUID = 1L;*/

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    public String name;
    public String surname;
    public String company;
    public String email;
    public String telephone;

    public static Finder<Long,Contact> find = new Finder<Long,Contact>(Long.class,Contact.class);

    public static List<Contact> all() {
        return find.all();
    }

    public static void create(Contact contact) {
        contact.save();
    }

    public static void delete(Long id) {
        find.ref(id).delete();
    }
}
