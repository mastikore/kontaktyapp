package controllers;

import models.Contact;
import play.Logger;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

public class Application extends Controller {

    static Form<Contact> contactForm = Form.form(Contact.class);

    public static Result index() {
        return redirect(routes.Application.contacts());
    }

    public static Result contacts() {
        return ok(
                views.html.widokKontaktow.render(Contact.all(), contactForm)
        );
    }

    public static Result newContact() {
        Form<Contact> filledForm = contactForm.bindFromRequest();
        if(filledForm.hasErrors()) {
            return badRequest(
                    views.html.widokKontaktow.render(Contact.all(), filledForm)
            );
        } else {
            Logger.info(filledForm.toString());
            Contact.create(filledForm.get());
            return redirect(routes.Application.contacts());
        }
    }

    public static Result deleteContact(Long id) {
        Contact.delete(id);
        return redirect(routes.Application.contacts());
    }
    public static Result editContact(Long id) {
        return TODO;
    }
}
